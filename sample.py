from pynamodb.models import Model
from pynamodb.attributes import UnicodeAttribute, NumberAttribute
import os

# Define AWS region and DynamoDB table name
AWS_REGION = 'eu-west-1'
TABLE_NAME = 'Users'

# Configure AWS credentials
os.environ['AWS_DEFAULT_REGION'] = AWS_REGION

class UserModel(Model):
    """
    A DynamoDB User Model
    """
    class Meta:
        table_name = TABLE_NAME
        region = AWS_REGION

    username = UnicodeAttribute(hash_key=True)
    first_name = UnicodeAttribute()
    last_name = UnicodeAttribute()
    age = NumberAttribute()

# Create the table
if not UserModel.exists():
    UserModel.create_table(read_capacity_units=1, write_capacity_units=1, wait=True)

# Add a new user
user = UserModel('john_doe', first_name='John', last_name='Doe', age=30)
user.save()

# Get a user
retrieved_user = UserModel.get('john_doe')
print(f"Retrieved User: {retrieved_user.first_name} {retrieved_user.last_name}, Age: {retrieved_user.age}")

# Update a user
retrieved_user.update(actions=[
    UserModel.first_name.set('Johnny'),
    UserModel.age.set(31)
])

# Delete a user
retrieved_user.delete()

