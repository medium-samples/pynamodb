# PynamoDB Example Project

This repository contains a practical example of using PynamoDB, a Pythonic interface to AWS DynamoDB. It demonstrates how to perform basic CRUD operations in a more intuitive and readable manner compared to traditional DynamoDB usage.

## Blog Post Reference

For a detailed explanation and insights on how PynamoDB can transform your DynamoDB interactions in Python, check out my blog post:
[Transforming DynamoDB Operations in Python with PynamoDB](https://medium.com/p/61b37910e0b2)

## Getting Started

### Prerequisites

-   Python 3.x
-   AWS account with configured credentials
-   DynamoDB access

### Installation

1. Clone the repository:

    ```bash
    git clone https://github.com/your-username/pynamodb-example.git
    cd pynamodb-example
    ```

2. Install requirements
   `pip install -r requirements.txt`

### Usage

The sample.py file in this repository contains the following operations:

-   Defining a DynamoDB model with PynamoDB
-   Creating a DynamoDB table
-   Adding, retrieving, updating, and deleting a user

to run the example, simply execute:

`python sample.py `

You should see output similar to:

`Retrieved User: John Doe, Age: 30`

## Contributing

Contributions to this project are welcome! Feel free to fork the repo, create a new branch, make your changes, and open a pull request.

## License

This project is licensed under the MIT License - see the LICENSE file for details.

## Contact

Your Name - [@AxeMindx](https://twitter.com/AxeMindx)
Project Link : https://gitlab.com/medium-samples/pynamodb
